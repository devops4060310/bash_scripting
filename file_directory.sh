#! /bin/bash
#1 Create directory & file
#echo "Enter Directory Name"
#read demo
#mkdir $demo
#echo "Enter File Name"
#read demo
#touch $demo.txt

#2 Check Directory exists or not
#echo "Enter Directory Name"
#read test
#if [ -d $test ]
#then
#	echo $test "directory already exists"
##	echo "Deleteing Directory.."
##	rm -rf $test
#else
#	echo $test "directory doesn't exists"
#	echo "Creating Directory" $test 
#	mkdir $test
#fi

#3 Check file are exists or not
echo "Enter File Name"
read file
if [ -f $file ]
then
	echo $file "File already exists"
	echo "Enter the text that want to append"
	read filetext
	echo $filetext >> $file   # append text
else
	echo $file "File doesn't exists"
	echo "Creating File" $file
	touch $file
fi

