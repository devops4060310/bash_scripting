#! /bin/bash

myarray=( 1 3 7 hello "good morning" )

echo "${myarray[0]}"

echo "${myarray[4]}"

echo "${myarray[2]}"

#how to find no. of values of array
echo "No. of values, length of array is ${#myarray[*]}"


echo "values from index 2-3 ${myarray[*]:2:3}"

# updateing our array with new values
myarray+=( new 70 "good afternoon")

echo "values of new array are ${myarray[*]}"

