#! /bin/bash

# How to store the key value pairs

declare -A myArray

myArray=( [name]=shashi [age]=27 [city]=mumbai )
echo "my name is ${myArray[name]}"

echo "Age is ${myArray[age]}"

echo "work location is ${myArray[city]}"
