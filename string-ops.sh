#! /bin/bash

myvar="hey buddy, how are you?"

myvarlength=${#myvar}
echo "Length is the myvar is $myvarlength"

# Upper& lower case
echo "upper case in myvar is ---->${myvar^^}"

echo "lower case in myvar is ---->${myvar,,}"

# How to replace string
echo "replace string in myvar ----> ${myvar/buddy/shashi}"

# TO slice string
echo "After slice myvar ---> ${myvar:4:15}"
