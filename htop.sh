#! /bin/bash

b=`which htop`

if [ $? == 0 ]
then
	echo "htop already installed"
	echo $b
else
	echo "htop not installed"
	sudo apt install htop -y
	echo $b
fi

